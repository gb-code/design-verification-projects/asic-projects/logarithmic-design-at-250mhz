##
## DESCRIPTION

Synthesized and Validated the synthesis problem to 250 Mhz
Modified the (log.v) problem so that it synthesizes correctly at 250 Mhz. 
Verified correct operation by running the test script(runlog.pl). 

##
## RUN COMMAND
./runlog.pl 4 results.txt

